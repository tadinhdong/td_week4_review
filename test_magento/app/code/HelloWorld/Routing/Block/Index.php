<?php
namespace HelloWorld\Routing\Block;

use Magento\Framework\View\Element\Template;

class Index extends Template
{


    public function getTitle()
    {
        return __('Information');
    }


    public function getName(){
        $name=$this->getRequest()->getParam('name', 'Động');
        return $name;
    }

    public function getDob(){
        $dob=$this->getRequest()->getParam('dob','06-08-2001');
        return $dob;
    }

    public function getAdd(){
        $address=$this->getRequest()->getParam('address', 'Nam Định');
        return $address;
    }

    public function getDes(){
        $description=$this->getRequest()->getParam('description','From Bss');
        return $description;
    }

}

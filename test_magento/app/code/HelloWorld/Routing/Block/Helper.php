<?php
namespace HelloWorld\Routing\Block;

use Magento\Framework\View\Element\Template;

class Helper extends Template
{
    protected $helperData;

    public function __construct(
        Template\Context $context,
        \HelloWorld\Routing\Helper\Data $helperData
    )
    {
        parent::__construct($context);
        $this->helperData = $helperData;
    }
    public function gettext()
    {
        $description= $this->helperData->getGeneralConfig('description');
        return $description;
    }

    public function getTitle()
    {
        $title= $this->helperData->getGeneralConfig('title');
        return $title;
    }

}

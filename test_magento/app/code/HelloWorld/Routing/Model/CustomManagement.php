<?php
namespace HelloWorld\Routing\Model;

/**
 * Class CustomManagement
 * @package HelloWorld\Routing\Model
 */
class CustomManagement implements \HelloWorld\Routing\Api\CustomManagementInterface
{
    /**
     * {@inheritdoc}
     */

    public function getList($id)
    {
        $response = [
            ['id'=>1,'name'=>"item1", 'class'=>"Class1"],
            ['id'=>2,'name'=>"item2", 'class'=>"Class2"],
            ['id'=>3,'name'=>"item3", 'class'=>"Class3"],
            ['id'=>4,'name'=>"item4", 'class'=>"Class4"],
            ['id'=>5,'name'=>"item5", 'class'=>"Class5"]
        ];

        try{
            $item= $response[$id];
        }catch(\Exception $e){
            $item=['error' => $e->getMessage()];
        }
        return $item;
    }
}

<?php
declare(strict_types=1);

namespace HelloWorld\Routing\Controller;

use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 */
class Bss implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * Router constructor.
     *
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->response = $response;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');

        if (strcmp($identifier, 'test1') === 0) {
            $request->setModuleName('bss');
            $request->setControllerName('index');
            $request->setActionName('helloworld');
        }else if(strcmp($identifier, 'test2') === 0){
            $request->setModuleName('bss');
            $request->setControllerName('router');
            $request->setActionName('testrequest');
        }else if(strcmp($identifier, 'test3') === 0){
            $request->setModuleName('bss');
            $request->setControllerName('router');
            $request->setActionName('testjson');
        }else if(strcmp($identifier, 'test4') === 0){
            $request->setModuleName('bss');
            $request->setControllerName('router');
            $request->setActionName('testforward');
        }else{
            $request->setModuleName('cms');
            $request->setControllerName('index');
            $request->setActionName('DefaultNoRoute');
        }
        $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);
        return $this->actionFactory->create(\Magento\Framework\App\Action\Forward::class);
    }
}

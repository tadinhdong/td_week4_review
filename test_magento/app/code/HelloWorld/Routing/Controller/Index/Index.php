<?php

namespace HelloWorld\Routing\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $helperData;
    protected $pageFactory;
    protected $resultRedirectFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \HelloWorld\Routing\Helper\Data $helperData,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
    )
    {
        parent::__construct($context);
        $this->helperData = $helperData;
        $this->pageFactory = $pageFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    public function execute()
    {
        if($this->helperData->getGeneralConfig('enable')==1){
            $page=$this->pageFactory->create();
            $page->getConfig()->getTitle()->set($this->helperData->getGeneralConfig('title'));
            return $page;
        }else{
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('no-router');
            return $resultRedirect;
        }
    }
}

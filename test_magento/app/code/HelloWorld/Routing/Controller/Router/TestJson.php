<?php
namespace HelloWorld\Routing\Controller\Router;

use Magento\Framework\App\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
/**
 * Class Json
 *
 * @package HelloWorld\Routing\Controller\Router
 */
class TestJson extends Action\Action
{
    protected $_resultJsonFactory;

    /**
     * RouterJson constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Return information
     *
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $params = [
            'Name' => "Động",
            "Dob" => "06-08-2001",
            "Address" => "Nam Định",
            "Description" => "BSS"
        ];
        return $result->setData($params);
    }
}

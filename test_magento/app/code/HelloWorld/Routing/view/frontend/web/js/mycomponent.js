
define(['jquery', 'uiComponent', 'ko'], function ($, Component, ko) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'HelloWorld_Routing/mycomponent'
            },
            initialize: function () {
                this.Data = ko.observableArray([]);
                this.nameData = ko.observable('John');
                this.priceData = ko.observable('21000');
                this.desData = ko.observable('Đẹp');
                this.linkData = ko.observable('https');
                this._super();
            },

            addNewCustomer: function () {
                this.Data.push({name:this.nameData(),price:this.priceData(),des:this.desData(),link:this.linkData()});
                this.nameData('');
                this.priceData('');
                this.desData('');
                this.linkData('');
            },

            removeSeat: function(seat){
                this.Data.remove(seat)
            },

        });
    }
);

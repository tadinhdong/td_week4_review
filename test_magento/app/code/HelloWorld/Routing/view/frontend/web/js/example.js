define([
    'jquery'
], function($) {
    'use strict';

    var exampleComponent = function(config, element) {
        $(document).ready(function() {
            //add
            $("button").click(function (e) {
                e.preventDefault();
                var datas=$("#form").serializeArray();
                var text='<tr>';
                $.each(datas, function( i, data ) {
                  text+='<td>'+data.value+'</td>'
               });
                text+='<td class="delete">delete</td></tr>'
                $( "#myTable" ).append(text);
            });

            //delete
            $("#myTable").on('click', '.delete', function () {
                $(this).closest('tr').remove();
            });
        });
    };

    return exampleComponent;
});

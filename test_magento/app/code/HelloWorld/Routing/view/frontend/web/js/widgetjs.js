define([
    'jquery',
    'Magento_Ui/js/modal/prompt',
    'jquery/ui'

], function ($) {
    'use strict';

    $.widget('example.mywidget', {

        _create: function () {
            this._addElement();
            this._delete();
        },

        _delete: function(){
            $("#myTable").on('click', '.delete', function () {
                let person = prompt("Are you sure","data");
                if (person != null) {
                    $(this).closest('tr').remove();
                }
            });
        },

        _addElement: function () {
            $("button").click(function (e) {
                e.preventDefault();
                var datas=$("#form").serializeArray();
                var text='<tr>';
                $.each(datas, function( i, data ) {
                    text+='<td>'+data.value+'</td>'
                });
                text+='<td class="delete">delete</td></tr>'
                $( "#myTable" ).append(text);
            });
        }
    });

    return $.example.mywidget;
});

